# Script to help Zanata Administrators

Zanata does not present a clear view around users and their projects in
the admin UI, thus it is difficult to reply to user requests.

This little script is a command-line tool presenting some extra information without altering the database in any way.

## Installation

Currently there is no real setup but you can run it in-place in a Python virtual environment (to avoid dirtying your system).

After cloning the repository:
```
cd zanata-admin
python3 -m venv .
source ./bin/activate
./bin/pip3 install peewee pymysql
```

Database creadentials needs to be put into `~/.my.cnf`:
```
[client]
user = <db-user>
password = <db-password>
```

## Usage

```
cd zanata-admin
source ./bin/activate
./zanata-admin --help
```

